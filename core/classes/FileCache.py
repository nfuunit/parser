import os
import hashlib
from core.interfaces import Cache


class FileCache(Cache):
    def __init__(self, fpath: str, name: str):
        def exists(_path: str) -> bool:
            return os.path.exists(_path) and os.path.isdir(_path)

        if not exists(fpath):
            raise Exception(f'Path {fpath} does not exists')
        cpath = os.path.join(fpath, 'cache', name)
        if not exists(cpath):
            os.makedirs(cpath, 0o775)
        self.__path = cpath

    def __get_file(self, key: str) -> str:
        filename = hashlib.md5(key.encode('utf8')).hexdigest()
        return os.path.join(self.__path, filename)

    def get(self, key: str) -> str | None:
        if not self.has(key):
            return None
        filename = self.__get_file(key)
        with open(filename) as file:
            return file.read()

    def has(self, key: str) -> bool:
        filename = self.__get_file(key)
        return os.path.exists(filename) and os.path.isfile(filename)

    def set(self, key: str, data: str = None):
        if not data:
            return self.delete(key)
        filename = self.__get_file(key)
        with open(filename, mode='w') as file:
            file.write(data)

    def delete(self, key: str):
        if not self.has(key):
            return
        filename = self.__get_file(key)
        os.remove(filename)
