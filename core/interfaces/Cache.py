class Cache:
    def get(self, key: str) -> str | None:
        pass

    def set(self, key: str, data: str = None):
        pass

    def has(self, key: str) -> bool:
        pass

    def delete(self, key: str):
        pass
