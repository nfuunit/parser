import os
import logging
import asyncio
import xml.dom.minidom as xmldom
import pika
import json
from bs4 import BeautifulSoup
from aiohttp import ClientSession, ClientTimeout, TCPConnector
from typing import Dict, List, Callable

from pika.adapters.blocking_connection import BlockingConnection

from core.classes import FileCache


logging.basicConfig(
    level=logging.INFO,
    format='[%(asctime)s] %(levelname)s - %(message)s',
    datefmt='%d-%b-%y %H:%M:%S'
)


BASE_URL = 'https://nfuunit.ru'
CACHE = FileCache(
    fpath=os.path.abspath('./'),
    name='uunit'
)


async def get_remote_data(url: str, session: ClientSession) -> str | None:
    if CACHE.has(url):
        logging.debug(f'FROM CACHE: {url}')
        return CACHE.get(url)
    try:
        logging.debug(f'REQUEST: {url}')
        return None
        response = await session.get(url)
        if not response.ok:
            return None
        text = await response.text()
        CACHE.set(url, text)
        return text
    except Exception as e:
        logging.warning(e)
        return None


async def get_links_to_parse(session: ClientSession) -> List:
    result = []
    response = await get_remote_data(
        url=BASE_URL + '/sitemap.xml',
        session=session
    )
    if not response:
        return result
    doc = xmldom.parseString(response)
    urls = doc.getElementsByTagName('url')
    for url in urls:
        loc = url.getElementsByTagName('loc')
        if not loc or not len(loc[0].childNodes):
            continue
        node = loc[0].childNodes[0]
        value = node.nodeValue
        if value:
            result.append(value)
    return result


def parse_news_item(content: str) -> Dict | None:
    html = BeautifulSoup(content, 'html.parser')
    base = html.find('main')
    if not base:
        return None
    item = {
        'title': None,
        'public_at': None,
        'images': list(),
        'content': None
    }
    header = base.find('h1')
    if header:
        date = header.find('span', {'class': 'date'})
        if date:
            item['public_at'] = date.text.strip()
            date.extract()
        item['title'] = header.text.strip()
    header.extract()
    image = base.find('div', {
        'class': ['image', 'image-content']
    })
    if image:
        image_link = image.find('a', {'class': 'modal'})
        if image_link:
            item['images'].append(image_link.get('href'))
        image.extract()
    description = base.find('section', {'class': 'description'})
    if description:
        photo_el = description.find('div', {'class': 'photo-image'})
        if photo_el:
            links = photo_el.find_all('a', {'class': 'modal'})
            if links:
                for link in links:
                    item['images'].append(link.get('href'))
            photo_el.extract()
        h2 = description.find_all('h2')
        if h2:
            for h in h2:
                if h.text.strip() == 'Фотографии':
                    h.extract()
        item['content'] = description.prettify()
    return item


async def main(rmq_connection: BlockingConnection):
    timeout = ClientTimeout(total=10)
    session = ClientSession(
        connector=TCPConnector(ssl=False),
        timeout=timeout
    )
    QUEUE_NEWS = 'save_to_news'
    QUEUE_PAGE = 'save_to_page'
    rmq_channel = rmq_connection.channel()
    rmq_channel.queue_declare(queue=QUEUE_PAGE)
    rmq_channel.queue_declare(queue=QUEUE_NEWS)

    async def parse_item(url: str, channel: str, callback: Callable = None):
        content = await get_remote_data(url, session)
        if content and callback:
            res = callback(content)
            if not res:
                return logging.warning(f'EMPTY DATA: {url}')
            try:
                logging.info(res.get('title'))
                res['url'] = url
                rmq_channel.basic_publish(
                    exchange='',
                    routing_key=channel,
                    body=bytes(
                        json.dumps(
                            res,
                            sort_keys=True,
                            indent=4
                        ),
                        'utf-8'
                    )
                )
            except Exception as e:
                logging.error({
                    'url': url,
                    'message': e,
                    'content': content
                })
        return

    links = await get_links_to_parse(session)
    logging.debug(f'Finded links: {len(links)}')
    tasks = []
    for link in filter(lambda l: '/news/n-' in l, links):
        if '/news/n-' in link:
            tasks.append(
                parse_item(
                    url=link,
                    channel=QUEUE_NEWS,
                    callback=parse_news_item
                )
            )
        elif '/p-' in link:
            tasks.append(
                parse_item(
                    url=link,
                    channel=QUEUE_PAGE,
                    callback=parse_news_item
                )
            )
        if len(tasks) >= 5:
            await asyncio.gather(*tasks)
            tasks = []
    await session.close()
    return logging.debug('DONE')


if __name__ == '__main__':
    RMQ_CONNECTION = pika.BlockingConnection(
        parameters=pika.URLParameters(
            url='amqp://admin:admin@127.0.0.1:5672'
        )
    )
    asyncio.run(main(RMQ_CONNECTION))
    RMQ_CONNECTION.close()
